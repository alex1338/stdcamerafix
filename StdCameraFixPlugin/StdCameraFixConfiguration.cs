﻿using Dalamud.Configuration;
using System;

namespace StdCameraFixPlugin
{
    [Serializable]
    public class StdCameraFixConfiguration : IPluginConfiguration
    {
        public int Version { get; set; }
    }
}
