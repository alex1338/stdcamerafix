﻿using Dalamud.Game;
using Dalamud.Hooking;
using Dalamud.Logging;
using StdCameraFixPlugin.Camera;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StdCameraFixPlugin
{
    public class StdCameraFixHandler
    {
        private unsafe static CameraManager* cameraManager = null;
        private unsafe static FFXIVClientStructs.FFXIV.Client.Game.ActionManager* actionManager;
        private unsafe delegate byte UseActionDelegate(FFXIVClientStructs.FFXIV.Client.Game.ActionManager* actionManager, uint actionType, uint actionID, long targetObjectID, uint param, uint useType, int pvp, bool* isGroundTarget);
        private unsafe Hook<UseActionDelegate> UseActionHook;

        private unsafe delegate void SetGameObjectOrientationDelegate(FFXIVClientStructs.FFXIV.Client.Game.Object.GameObject* gameObject, float orientation);
        private unsafe Hook<SetGameObjectOrientationDelegate> SetGameObjectOrientationHook;

        private static Dictionary<uint, Lumina.Excel.GeneratedSheets.Action> actionSheet = null!;

        private bool performingAction = false;

        public StdCameraFixHandler()
        {
            actionSheet = StdCameraFixPlugin.DataManager?.GetExcelSheet<Lumina.Excel.GeneratedSheets.Action>()?.Where(i => i.ClassJobCategory.Row > 0 && i.ActionCategory.Row <= 4 && i.RowId is not 7)?.ToDictionary(i => i.RowId, i => i)!;

            if (actionSheet == null)
                throw new ApplicationException("Action sheet failed to load!");

            unsafe
            {
                actionManager = FFXIVClientStructs.FFXIV.Client.Game.ActionManager.Instance();
                cameraManager = (CameraManager*)StdCameraFixPlugin.TargetModuleScanner.GetStaticAddressFromSig("48 8D 35 ?? ?? ?? ?? 48 8B 09");

                UseActionHook = new Hook<UseActionDelegate>(StdCameraFixPlugin.TargetModuleScanner.ScanText("E8 ?? ?? ?? ?? 89 9F 14 79 02 00"), UseAction);
                UseActionHook.Enable();

                SetGameObjectOrientationHook = new Hook<SetGameObjectOrientationDelegate>(StdCameraFixPlugin.TargetModuleScanner.ScanText("E8 ?? ?? ?? ?? 83 FE 4F"), SetGameObjectOrientation);
                SetGameObjectOrientationHook.Enable();
            }
        }

        private unsafe byte UseAction(FFXIVClientStructs.FFXIV.Client.Game.ActionManager* actionManager, uint actionType, uint actionID, long targetObjectID, uint param, uint useType, int pvp, bool* isGroundTarget)
        {
            try
            {
                uint adjusted = actionType == 1 ? actionManager->GetAdjustedActionId(actionID) : actionID;
                float preActionCameraRotation = 0;

                if (actionType == 1 && GetActionStatus(actionType, adjusted) == 0 && actionSheet.ContainsKey(adjusted))
                {
                    var a = actionSheet[adjusted];
                    if (a.CanTargetHostile)
                    {
                        if (StdCameraFixPlugin.TargetManager.Target != null && StdCameraFixPlugin.ClientState.LocalPlayer != null)
                        {
                            // Save camera rotation before a targetting action is performed
                            preActionCameraRotation = StdCameraFixPlugin.ClientState.LocalPlayer.Rotation;
                            performingAction = true;
                        }
                    }
                }

                byte ret = UseActionHook.Original(actionManager, actionType, actionID, targetObjectID, param, useType, pvp, isGroundTarget);

                if (actionType == 1 && GetActionStatus(actionType, adjusted) != 0 && actionSheet.ContainsKey(adjusted))
                {
                    var a = actionSheet[adjusted];
                    if (a.CanTargetHostile && performingAction)
                    {
                        // Set player rotation to saved camera rotation
                        SetCharacterRotationToCamera(preActionCameraRotation - MathF.PI);
                        performingAction = false;
                    }
                }

                return ret;
            }
            catch (Exception e)
            {
                PluginLog.Error($"Failed to modify action\n{e}");
                return 0;
            }
        }

        private unsafe void SetGameObjectOrientation(FFXIVClientStructs.FFXIV.Client.Game.Object.GameObject* gameObject, float orientation)
        {
            try
            {
                if (StdCameraFixPlugin.ClientState.LocalPlayer != null && cameraManager != null && cameraManager->WorldCamera->ControlType == 2)
                {
                    var localPlayer = (FFXIVClientStructs.FFXIV.Client.Game.Object.GameObject*)StdCameraFixPlugin.ClientState.LocalPlayer!.Address;

                    if (gameObject == localPlayer)
                    {
                        if (!performingAction && StdCameraFixPlugin.TargetManager.Target != null &&
                            StdCameraFixPlugin.ClientState.LocalPlayer.StatusFlags.HasFlag(Dalamud.Game.ClientState.Objects.Enums.StatusFlags.InCombat))
                        {
                            float camRotation = GetCameraRotation();
                            if (camRotation >= 0)
                                camRotation -= MathF.PI;
                            else
                                camRotation += MathF.PI;
                            const float floatMaxDiff = 2.5f * MathF.PI / 180.0f;

                            // Trying to rotate the player by more than floatMaxDiff degrees in respect to the camera rotation while in combat
                            // will result in the player rotation being reset to the camera rotation
                            if (Math.Abs(orientation - camRotation) > floatMaxDiff && Math.Abs(orientation - camRotation) < 2 * MathF.PI - floatMaxDiff)
                            {
                                orientation = camRotation;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                PluginLog.Error($"Failed to modify SetGameObjectOrientation\n{e}");
            }

            SetGameObjectOrientationHook.Original(gameObject, orientation);
        }

        public void Update(Framework framework)
        {
            try
            {
                // Maybe do something here?
            }
            catch (Exception e)
            {
                PluginLog.Error($"Failed to Update\n{e}");
            }
        }

        public unsafe static float GetCameraRotation() => cameraManager->WorldCamera->CurrentHRotation;

        public unsafe void SetCharacterRotationToCamera(float hRotation)
        {
            var localPlayer = (FFXIVClientStructs.FFXIV.Client.Game.Object.GameObject*)StdCameraFixPlugin.ClientState.LocalPlayer!.Address;
            SetGameObjectOrientation(localPlayer, hRotation + MathF.PI);
        }

        public unsafe static uint GetActionStatus(uint actionType, uint actionID, long targetObjectID = 0xE000_0000, byte checkCooldown = 1, byte checkCasting = 1)
        {
            var func = (delegate* unmanaged[Stdcall]<FFXIVClientStructs.FFXIV.Client.Game.ActionManager*, uint, uint, long, uint, uint, uint>)FFXIVClientStructs.FFXIV.Client.Game.ActionManager.fpGetActionStatus;
            return func(actionManager, actionType, actionID, targetObjectID, checkCooldown, checkCasting);
        }

        public void Dispose()
        {
            UseActionHook?.Dispose();
            SetGameObjectOrientationHook?.Dispose();
        }
    }
}
