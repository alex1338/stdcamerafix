﻿using Dalamud.Data;
using Dalamud.Game;
using Dalamud.Game.ClientState;
using Dalamud.Game.ClientState.Objects;
using Dalamud.Game.Command;
using Dalamud.IoC;
using Dalamud.Plugin;

namespace StdCameraFixPlugin
{
    class StdCameraFixPlugin : IDalamudPlugin
    {
        public string Name => "Standard Camera Fix Plugin";

        public StdCameraFixConfiguration Configuration;

        private StdCameraFixHandler camFixHandler;

        [PluginService] public static CommandManager CommandManager { get; private set; } = null!;
        [PluginService] public static DalamudPluginInterface PluginInterface { get; private set; } = null!;
        [PluginService] public static SigScanner TargetModuleScanner { get; private set; } = null!;
        [PluginService] public static ClientState ClientState { get; private set; } = null!;
        [PluginService] public static TargetManager TargetManager { get; private set; } = null!;
        [PluginService] public static ObjectTable ObjectTable { get; private set; } = null!;
        [PluginService] public static Framework Framework { get; private set; } = null!;
        [PluginService] public static DataManager DataManager { get; private set; } = null!;

        public StdCameraFixPlugin()
        {
            this.Configuration = PluginInterface.GetPluginConfig() as StdCameraFixConfiguration ?? new StdCameraFixConfiguration();
            if (Configuration.Version < 4)
            {
                Configuration.Version = 4;
            }

            FFXIVClientStructs.Resolver.Initialize();

            this.camFixHandler = new StdCameraFixHandler();

            Framework.Update += Update;
        }

        private void Update(Framework framework) => camFixHandler.Update(framework);

        public void Dispose()
        {
            camFixHandler.Dispose();
        }
    }
}