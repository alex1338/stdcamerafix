﻿using System.Runtime.InteropServices;

namespace StdCameraFixPlugin.Camera
{
    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct CameraManager
    {
        [FieldOffset(0x0)] public Camera* WorldCamera;
        [FieldOffset(0x8)] public Camera* IdleCamera;
        [FieldOffset(0x10)] public Camera* MenuCamera;
        [FieldOffset(0x18)] public Camera* SpectatorCamera;
    }
}
